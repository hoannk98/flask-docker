#!/usr/bin/env bash

cd `dirname $BASH_SOURCE`

echo "image name: $CI_REGISTRY_IMAGE"
docker-compose stop
docker-compose rm -f
docker-compose pull
docker-compose up -d

cd - > /dev/null
